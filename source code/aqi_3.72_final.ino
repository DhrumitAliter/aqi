#include <ModbusMaster.h>
#include <SoftwareSerial.h>
#include <WiFi.h>
#include <Nextion.h>
#include <FS.h>
#include <SPIFFS.h>
#include <Wire.h>
#include <HTTPClient.h>
#include <sps30.h>
#include "soc/soc.h" //disable brownour problems
#include "soc/rtc_cntl_reg.h"  //disable brownour problems
#include "SparkFun_SCD30_Arduino_Library.h" //Click here to get the library: http://librarymanager/All#SparkFun_SCD30
#include "Adafruit_SGP30.h"
#include "Update.h"
#include "time.h"
#include "EasyBuzzer.h"
#include "MAX1704X.h"

SCD30 airSensor;
Adafruit_SGP30 sgp;
MAX1704X _fuelGauge1 = MAX1704X(5);
MAX1704X _fuelGauge2 = MAX1704X(10);

SoftwareSerial rs485Serial(14, 26); // RX, TX
#define MASTER_EN   27   // connected to RS485 Enable pin
ModbusMaster node;


TaskHandle_t Task1;
TaskHandle_t Task2;

bool wifiStatus = false;
bool connectWifiStatus = false;
bool isConnected = false;

const double FW_VERSION = 3.7;
const char* fwUrlBase = "https://beta.alitersolutions.com/demo/images/OTA/aqiOTA/";
String fwURL = "";
String baseUrl =  "http://api.iflowindia.com/api/device/manual?";

int buzzerPin = 13;
int wifiPic;
long rssi;
int co2, temp, hum, pm25, pm1, pm10, aqi;
float tvoc;
unsigned long oldMillis = 0, oldMillisSensor = 0, oldMillisBtry = 0, oldMillisMdbs = 0;

unsigned int frequency = 2000;
unsigned int onDuration = 200;
unsigned int offDuration = 200;
unsigned int beeps = 3;
unsigned int pauseDuration = 300;
unsigned int cycles = 10;


uint64_t chipid;
String iKonnectId = "";
char* deviceID;

const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 19800;
const int   daylightOffset_sec = 3600;

String months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
String hr, mins, sec, dd, yyyy;
int mm;
uint32_t hourAlarm, minAlarm;
NexButton wifiButton = NexButton(3, 2, "wifiButton");
NexButton connectWiFi = NexButton(5, 13, "connectWiFi");
NexButton FrgtWifi = NexButton(4, 13, "disconWifi");
NexButton about = NexButton(3, 7, "aboutButton");
//NexButton screentest = NexButton(1, 6, "screentest");
//NexButton screenOff = NexButton(11, 13, "screenOff");
NexButton screenOK = NexButton(12, 13, "screenOK");
NexButton m2mButton = NexButton(3, 3, "m2mButton");
NexButton updateOTA = NexButton(3, 6, "updateButton");
NexButton dataStatusButton = NexButton(10, 13, "dataStatus");
NexButton confirmReset = NexButton(7, 13, "confirmReset");
NexButton alarmOK = NexButton(14, 13, "alarmOK");
NexButton alarmButton = NexButton(3, 10, "alarmButton");
NexButton alarmOff = NexButton(14, 25, "alarmOff");
NexDSButton turnAlarm = NexDSButton(13, 19, "turnAlarm");

NexText login = NexText(5, 17, "login");
NexText pass = NexText(5, 18, "pass");
NexText wifiName = NexText(4, 15, "wifiName");

NexNumber screenTime = NexNumber(12, 14, "ScreenTime");
NexNumber alarmHour = NexNumber(14, 14, "alarmHour");
NexNumber alarmMin = NexNumber(14, 22, "alarmMin");
NexTouch *nex_listen_list[] = {
  &updateOTA, &turnAlarm, &alarmOK, &alarmOff, &alarmButton, &confirmReset, &screenTime, &login, &pass, &connectWiFi, &FrgtWifi, &wifiButton, &about, &screenOK, &m2mButton, &dataStatusButton, NULL
};

char ssidBuffer[100] = {0};
char passBuffer[100] = {0};
char secBuffer[10] = {0};

String wifiSSID = "";
String wifiPASS = "";


void endNextionCommand() {
  Serial2.write(0xff);
  Serial2.write(0xff);
  Serial2.write(0xff);
}

void printLocalTime()
{
  struct tm timeinfo;
  if (!getLocalTime(&timeinfo)) {
    Serial.println("Failed to obtain time");
    return;
  }
  //Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
  hr = String(timeinfo.tm_hour);
  mins = String(timeinfo.tm_min);
  //  String sec=String(timeinfo.tm_sec);
  String locTime = String(hr + ":" + mins);

  dd = String(timeinfo.tm_mday);
  //String mm=String(timeinfo.tm_mon+1);
  String yyyy = String(timeinfo.tm_year + 1900);
  mm = timeinfo.tm_mon;

  String locDate = String(dd + " " + months[mm]);
  //  Serial.print(locTime); Serial.print(" "); Serial.println(locDate);
  String dateCMD = "date.txt=\"" + String(locDate) + "\"";
  Serial2.print(dateCMD);
  endNextionCommand();
  String timeCMD = "time.txt=\"" + String(locTime) + "\"";
  Serial2.print(timeCMD);
  endNextionCommand();

}

void readFile(fs::FS &fs, const char * path) {
  Serial.printf("Reading file: %s\r\n", path);

  File file = fs.open(path);
  if (!file || file.isDirectory()) {
    Serial.println("- failed to open file for reading");
    return;
  }

  Serial.println("- read from file:");
  //    Serial.println(slurp("/dhrumit.txt"));
  while (file.available()) {
    Serial.write(file.read());
  }
}
void deleteFile(fs::FS &fs, const char * path) {
  Serial.printf("Deleting file: %s\r\n", path);
  if (fs.remove(path)) {
    Serial.println("- file deleted");
  } else {
    Serial.println("- delete failed");
  }
}
String spiffRead(const String fileName)
{
  File f = SPIFFS.open(fileName, "r");
  String r = f.readString();
  f.close();
  return r;
}

void spiffWrite(const String fileName, const String content)
{
  File f = SPIFFS.open(fileName, "w");
  f.print(content);
  f.close();
}

void spiffDelete(const String fileName)
{
  SPIFFS.remove(fileName.c_str());
}


void displayFuelGauge(MAX1704X fuelGauge) {
  Serial.print("Version:       ");
  Serial.println(fuelGauge.version());
  Serial.print("ADC:           ");
  Serial.println(fuelGauge.adc());
  Serial.print("Voltage:       ");
  Serial.print(fuelGauge.voltage());
  Serial.println(" v");
  Serial.print("Percent:       ");
  int charge = fuelGauge.percent();
  Serial.print(charge);
  Serial.println("%");
  Serial.print("Is Sleeping:   ");
  //  Serial.println(fuelGauge.isSleeping() ? "Yes" : "No");
  //  Serial.print("Alert:         ");
  //  Serial.println(fuelGauge.alertIsActive() ? "Yes" : "No");
  Serial.print("Threshold:     ");
  Serial.println(fuelGauge.getThreshold());
  Serial.print("Compensation:  0x");
  Serial.println(fuelGauge.compensation(), HEX);

  //  int sensorValue;
  int batteryPic;
  //  int charge = 75;
  if (charge >= 100) {
    batteryPic = 24;
  } else if (charge < 100 & charge >= 60) {
    batteryPic = 23;
  } else if (charge < 60 & charge >= 40) {
    batteryPic = 22;
  } else if (charge < 40 & charge >= 5) {
    batteryPic = 21;
  } else  if (charge < 5) {
    batteryPic = 20;
  }
  String Command = "btryPic.pic=" + String(batteryPic);
  Serial2.print(Command);
  endNextionCommand();
}

bool wifiConnectStatus()
{
  bool wifiState;
  if (WiFi.status() == WL_CONNECTED) {
    wifiState = true;
    wifiSignalDisplay();

  } else {
    wifiState = false;
    String wifiCommand = "wifiPic.pic=" + String(18);
    Serial2.print(wifiCommand);
    endNextionCommand();
  }
  return wifiState;
}
void connectWiFi_Press(void *ptr) {
  connectWifiStatus = true;
  Serial.println("connectWiFi_Press");
  wifiSSID = "";
  wifiPASS = "";
  //  memset(buffer, 0, sizeof(buffer));
  login.getText(ssidBuffer, sizeof(ssidBuffer));
  pass.getText(passBuffer, sizeof(passBuffer));
  //String wifi=   "{\"wifiSSID\":\""+String(ssidBuffer)+"\",\"wifiPASS\":\""+String(passBuffer)+"\"}"  ;
  spiffWrite("/wifiSSID", ssidBuffer);
  spiffWrite("/wifiPASS", passBuffer);
  connectWifi();
}
void wifiButton_Press(void *ptr) {
  wifiStatus = true;
  if (wifiConnectStatus() == true) {
    Serial2.write("page FrgtWifiPopUp"); endNextionCommand();
  } else if (wifiConnectStatus() == false) {
    Serial2.write("page wifiPopUp"); endNextionCommand();
  }
  // Serial.println("wifiButton_Press");
  //// Serial.println(wifiSSID);
  // String wifiCMD = "connectedTo.txt=\""+String(wifiSSID)+"\"";
  //    Serial2.print(wifiCMD);
  //       endNextionCommand();
}
void connectWifi() {
  //  if(spiffExists("/wifiConfig"))
  //  {
  wifiSSID = spiffRead("/wifiSSID");
  wifiPASS = spiffRead("/wifiPASS");
  Serial.println(wifiSSID);
  Serial.println(wifiPASS);
  setupWifi();

}
bool dataStatus = false;
void m2mButton_Press(void *ptr) {
  Serial.println("m2mButton_Press");
  String a = "";
  if (dataStatus == true) {
    a = "ON";
    Serial.println(a);
  } else if (dataStatus == false) {
    a = "OFF";
    Serial.println(a);
  }
  String Command = "dataStatus.txt=\"" + String(a) + "\"";
  Serial2.print(Command);
  endNextionCommand();

}
void dataStatusButton_Press(void *ptr) {
  Serial.println("dataStatus_Press   ");
  dataStatus = !dataStatus;
  Serial.println(dataStatus);
  String a = "";
  if (dataStatus == true) {
    a = "ON";
    //    sendToModbus();
    Serial.println(a);
  } else if (dataStatus == false) {
    a = "OFF";
    Serial.println(a);
  }
  String Command = "dataStatus.txt=\"" + String(a) + "\"";
  Serial2.print(Command);
  endNextionCommand();
  //  dataStatu();
}


void preTransmission() {           //Function for setting stste of Pins DE & RE of RS-485{
  digitalWrite(MASTER_EN , HIGH);
}
void postTransmission() {
  digitalWrite(MASTER_EN , LOW);
}
void sendToModbus() {
  //  double tvo = tvoc * 22400 / 92.14f;
  node.writeSingleRegister(0x40000, pm25);
  node.writeSingleRegister(0x40001, pm1);
  node.writeSingleRegister(0x40002, pm10);
  node.writeSingleRegister(0x40003, temp);
  node.writeSingleRegister(0x40004, hum);
  node.writeSingleRegister(0x40005, co2);
  node.writeSingleRegister(0x40006, tvoc);
}

void setupWifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(wifiSSID);

  WiFi.mode(WIFI_STA);
  WiFi.begin(wifiSSID.c_str(), wifiPASS.c_str());
  String a = "Connecting...";
  String Com = "status.txt=\"" + String(a) + "\"";
  Serial2.print(Com);
  endNextionCommand();

  //  if (WiFi.config(staticIP, gateway, subnet, dns, dns) == false) {
  //    Serial.println("Configuration failed.");
  //  }
  //String b="";
  if (WiFi.status() == WL_CONNECTED)
  {
    Serial.println("in setup wifi");
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
    String b = "Connected ";
    String Command1 = "status.txt=\"" + String(b) + "\"";
    Serial2.print(Command1);
    endNextionCommand();

  } else {
    Serial.println(" failed.");
    String b = "Failed";
    String Command = "status.txt=\"" + String(b) + "\"";
    Serial2.print(Command);
    endNextionCommand();
  }
}
void FrgtWifi_Press(void *ptr) {
  Serial.println("FrgtWifi_Press");
  spiffDelete("/wifiSSID");
  spiffDelete("/wifiPASS");
  WiFi.disconnect();
  wifiSSID = "";
  wifiPASS = "";
  login.setText("");
  pass.setText("");
}
void wifiSignalDisplay() {
  rssi = WiFi.RSSI();
  //  Serial.print("RSSI:");
  //  Serial.println(rssi);

  if (rssi > -55) {
    wifiPic = 17;
  } else if (rssi < -55 & rssi > -65) {
    wifiPic = 16;
  } else if (rssi < -65 & rssi > -70) {
    wifiPic = 15;
  } else if (rssi < -70 & rssi > -78) {
    wifiPic = 14;
  } else if (rssi < -78 & rssi > -82) {
    wifiPic = 19;
    //  } else {
    //    wifiPic = 0;
  }
  String wifiCommand = "wifiPic.pic=" + String(wifiPic);
  Serial2.print(wifiCommand);
  endNextionCommand();
}


void updateESP() {
  File file = SPIFFS.open("/aqiOTA.bin");

  if (!file) {
    Serial.println("Failed to open file for reading");
    return;
  }

  Serial.println("Starting update..");

  String a = "Starting update...";
  String Command = "updateStatus.txt=\"" + String(a) + "\"";
  Serial2.print(Command); endNextionCommand();

  String l = "Starting reset...";
  String Commn2 = "resetStatus.txt=\"" + String(l) + "\"";
  Serial2.print(Commn2);
  endNextionCommand();
  //      String Command = "tempData.txt=\""+String(temp)+"\"";
  //    Serial2.print(Command);
  //       endNextionCommand();

  size_t fileSize = file.size();
  Serial.print("File size ");
  Serial.println(fileSize);
  if (!Update.begin(fileSize)) {

    Serial.println("Cannot do the update");

    return;
  };

  Update.writeStream(file);

  if (Update.end()) {

    Serial.println("Successful update");
    String d = "Successful update";
    String Command2 = "updateStatus.txt=\"" + String(d) + "\"";
    Serial2.print(Command2);
    endNextionCommand();
    String y = "Reset Successfull";
    String Comand2 = "resetStatus.txt=\"" + String(y) + "\"";
    Serial2.print(Comand2);
    endNextionCommand();


  } else {
    String q = "Error Occurred: " + String(Update.getError());
    Serial.println(q);
    String Comman2 = "updateStatus.txt=\"" + String(q) + "\"";
    Serial2.print(Comman2);
    endNextionCommand();
    String Cmman2 = "resetStatus.txt=\"" + String(q) + "\"";
    Serial2.print(Cmman2);
    endNextionCommand();
    return;
  }

  file.close();

  Serial.println("Reset in 4 seconds...");
  String c = "Reset in 4 seconds...";
  String Command1 = "updateStatus.txt=\"" + String(c) + "\"";
  Serial2.print(Command1);
  endNextionCommand();
  String Comand1 = "resetStatus.txt=\"" + String(c) + "\"";
  Serial2.print(Comand1);
  endNextionCommand();


  delay(4000);
  Serial2.print("rest"); endNextionCommand();
  ESP.restart();
}
void downloadEsp() {

  deleteFile(SPIFFS, "/aqiOTA.bin");
  File file = SPIFFS.open("/aqiOTA.bin", "w");
  String fwImageURL = fwURL;
  fwImageURL.concat( ".bin" );
  Serial.print( "Firmware bin URL: " );
  Serial.println( fwImageURL );

  HTTPClient http;


  http.begin(fwImageURL); //Specify the URL and certificate
  int httpCode = http.GET();
  Serial.println(httpCode);
  if (httpCode == HTTP_CODE_OK) { //Check for the returning code
    Serial.println("Downloading...");
    String h = "Downloading...";
    String Command = "updateStatus.txt=\"" + String(h) + "\"";
    Serial2.print(Command); endNextionCommand();
    http.writeToStream(&file);
  }
  size_t fileSize = file.size();
  file.close();
  http.end(); //Free the resources
  if (fileSize > 0) {
    String g = "Finished Downloading...";
    String Command9 = "updateStatus.txt=\"" + String(g) + "\"";
    Serial2.print(Command9); endNextionCommand();
    Serial.println("you have finished downloading");
  }
  else {
    Serial.println("Downloading Error");
  }
  delay(1000);

  updateESP();
}
void checkForUpdates() {
  String mac = iKonnectId;
  fwURL = String( fwUrlBase );
  fwURL.concat("aqiOTA");
  String fwVersionURL = fwURL;
  fwVersionURL.concat( ".txt" );

  Serial.println( "Checking for firmware updates." );
  Serial.print( "MAC address: " );
  Serial.println( mac );
  Serial.print( "Firmware version URL: " );
  Serial.println( fwVersionURL );
  String a = "Checking for update";
  String Command3 = "updateStatus.txt=\"" + String(a) + "\"";
  Serial2.print(Command3); endNextionCommand();

  HTTPClient httpClient;
  httpClient.begin(fwVersionURL );
  int httpCode = httpClient.GET();
  if ( httpCode == 200 ) {
    String newFWVersion = httpClient.getString();

    Serial.print( "Current firmware version: " );
    Serial.println( FW_VERSION );
    Serial.print( "Available firmware version: " );
    Serial.println( newFWVersion );

    float newVersion = newFWVersion.toFloat();
    Serial.println(newVersion);

    if ( newVersion > FW_VERSION ) {
      String b = "New Version Available";
      String Command = "updateStatus.txt=\"" + String(b) + "\"";
      Serial2.print(Command);
      endNextionCommand();
      Serial.println( "Preparing to update" );


      String e = "Preparing to update";
      String Command3 = "updateStatus.txt=\"" + String(e) + "\"";
      Serial2.print(Command3);
      endNextionCommand();
      downloadEsp();

    }

    else {
      Serial.println( "Already on latest version" );
      String f = "Already on latest version";
      String Command5 = "updateStatus.txt=\"" + String(f) + "\"";
      Serial2.print(Command5);
      endNextionCommand();

    }
  }
  else {
    Serial.print( "Firmware version check failed, got HTTP response code " );
    Serial.println( httpCode );
    String z = "ERROR:    " + String(httpCode);
    String Command = "updateStatus.txt=\"" + String(z) + "\"";
    Serial2.print(Command);
    endNextionCommand();
  }
  httpClient.end();
  delay(1000);
  //  updateESP();
}

void updateOTA_Press(void *ptr) {
  Serial.println("updateOTA_Press  ");
  checkForUpdates();
}

void screenOK_Press(void *ptr) {
  Serial.println("screenOK_Press   ");
  uint32_t sec;
  screenTime.getValue(&sec);
  Serial.println(sec);
  String Command = "thsp=" + String(sec);
  Serial2.print(Command);
  endNextionCommand();
  delay(500);
  Serial2.write("page setti"); endNextionCommand();

}

bool alarmState = false;
void alarmOK_Press(void *ptr) {
  Serial.println("alarmOK_Press   ");

  alarmHour.getValue(&hourAlarm);

  Serial.println(hourAlarm);
  alarmMin.getValue(&minAlarm);

  Serial.println(minAlarm);

  if (hourAlarm >= 0 && hourAlarm <= 23) {
    Serial.println("in hourAlarm");
    if (minAlarm >= 0 && minAlarm <= 59) {
      Serial.println("in minAlarm");
      alarmState = true;
      Serial2.print("page setti"); endNextionCommand();
    }
  }
  else {
    Serial.println("Invalid ");
    String CMD2 = "alarmStatus.txt=\"" + String("Enter Valid Time") + "\"";
    Serial2.print(CMD2); endNextionCommand();
    //    delay(1000);

  }

}


void turnAlarm_release(void *ptr)
{

  uint32_t dual_state;
  NexDSButton *btn = (NexDSButton *)ptr;
  //    dbSerialPrintln("b0PopCallback");
  //    dbSerialPrint("ptr=");
  //    dbSerialPrintln((uint32_t)ptr);
  //    memset(buffer, 0, sizeof(buffer));

  /* Get the state value of dual state button component . */
  turnAlarm.getValue(&dual_state);
  Serial.println(dual_state);

  if (dual_state)
  {
    Serial.println("OPEN");
    String a = "Turn Off";
    String Command1 = "turnAlarm.txt=\"" + String(a) + "\"";  Serial2.print(Command1); endNextionCommand();
    Serial2.print("page alarmPage2"); endNextionCommand();

  }
  else
  { String a = "Turn On";
    String Command2 = "turnAlarm.txt=\"" + String(a) + "\"";
    Serial2.print(Command2);
    endNextionCommand();
    Serial.println("OFF");
  }

  String CMD2 = "alarmStatus.txt=\"" + String("Enter Time") + "\"";
  Serial2.print(CMD2);
  endNextionCommand();
}

void alarmButton_Press(void *ptr) {
  Serial.println("alarmButton_Press   ");
  String CMD2 = "alarmStatus.txt=\"" + String("Enter Time") + "\"";
  Serial2.print(CMD2);
  endNextionCommand();
  if (alarmState == true) {
    String a = "Turn Off";
    String Command2 = "turnAlarm.txt=\"" + String(a) + "\"";
    Serial2.print(Command2);
    endNextionCommand();
  } else {
    String a = "Turn On";
    String Command2 = "turnAlarm.txt=\"" + String(a) + "\"";
    Serial2.print(Command2);
    endNextionCommand();
  }

}
void alarmOff_Press(void *ptr) {
  Serial.println("alarmOff_Press   ");
  alarmState = false;
  Serial2.write("page alarmPage"); endNextionCommand();
  String a = "Turn On";
  String Command2 = "turnAlarm.txt=\"" + String(a) + "\"";
  Serial2.print(Command2);
  endNextionCommand();
}

void DeviceID()
{
  //Getting ESP ChipID and storing it
  chipid = ESP.getEfuseMac();
  uint16_t long1 = (uint16_t)(chipid >> 32);
  uint32_t long2 = (uint32_t)chipid;
  iKonnectId = String(long1, HEX) + String(long2, HEX);
  Serial.println("ESP32 Chip Id : " + iKonnectId);
  iKonnectId.toCharArray(deviceID, iKonnectId.length());
}
void about_Press(void *ptr) {
  Serial.println("about_Press");
  Serial.println(iKonnectId);
  String Command = "mac.txt=\"" + String(iKonnectId) + "\"";
  Serial2.print(Command);
  endNextionCommand();
  //String ver="V1.0";
  String verCMD = "ver.txt=\"" + String(FW_VERSION) + "\"";
  Serial2.print(verCMD);
  endNextionCommand();
}
void confirmReset_Press(void *ptr) {
  Serial.println("reset_Press   ");
  //  dataStatu();
  updateESP();

}
void sendToServer() {
  if (WiFi.status() == WL_CONNECTED) {

    String url = baseUrl + "id="
                 + String(iKonnectId) +
                 "&aqi=" + String(aqi) +
                 "&co=1&co2=" + String(co2) +
                 "&h=" + String(hum) +
                 "&no2=1&o3=1&p=1&pm10=" + String(pm10) +
                 "&pm25=" + String(pm25) +
                 "&pm1=" + String(pm1) +
                 "&tv=" + String(tvoc) +
                 "&so2=1&t=" + String(temp) +
                 "&w=1";

    Serial.println(url);

    HTTPClient http;
    http.begin(url); //HTTP
    int httpCode = http.GET();
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] GET... code: %d\n", httpCode);

      // file found at server
      if (httpCode == HTTP_CODE_OK) {
        String payload = http.getString();
      }
    } else {
      Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
  }

}
void readPmSensor() {
  struct sps30_measurement m;
  char serial[SPS30_MAX_SERIAL_LEN];
  uint16_t data_ready;
  int16_t ret;

  do
  {
    ret = sps30_read_data_ready(&data_ready);
    if (ret < 0)
    {
      Serial.print("error reading data-ready flag: ");
      Serial.println(ret);
    }
    else if (!data_ready)
      Serial.print("data not ready, no new measurement available\n");
    else
      break;
    delay(100); /* retry in 100ms */
  }
  while (1);

  ret = sps30_read_measurement(&m);
  if (ret < 0)
  {
    Serial.print("error reading measurement\n");
  }
  else
  {

    
    //    Serial.println(m.mc_1p0);
    int pm1Temp,pm25Temp,pm10Temp;
    pm1Temp=m.mc_1p0;
    pm25Temp= m.mc_2p5 + 15;
    pm10Temp= m.mc_10p0 + 30;
    if(pm1Temp<=100){
      pm1=pm1Temp;
    } else{
      pm1=pm1Temp+((pm1Temp*10)/100);
//       pm1=pm1Temp+((pm1Temp*5)/100);
    }
     if(pm25Temp<=100){
      pm25=pm25Temp;
    } else{
      pm25=pm25Temp+((pm25Temp*10)/100);
//        pm25=pm25Temp+((pm25Temp*15)/100);
    }
     if(pm10Temp<=100){
      pm10=pm10Temp;
    } else{
//      pm10=pm10Temp+((pm10Temp*10)/100);
  pm10=pm10Temp+((pm10Temp*30)/100);
    }

    Serial.print("PM  1.0: ");Serial.println(pm1);
      Serial.print("PM  2.5: ");Serial.println(pm25);
      
////    pm1 = m.mc_1p0; Serial.println(pm1);
//    Serial.print("PM  2.5: ");
//    //    Serial.println(m.mc_2p5+15);
//    pm25 = m.mc_2p5 + 15; Serial.println(pm25);
//
//    Serial.print("PM 10.0: ");
//    pm10 = m.mc_10p0 + 30; Serial.println(m.mc_10p0 + 30);

    if (pm25 <= 30) {
      aqi = pm25 * 1.66;
    }
    else if (pm25 > 30 && pm25 <= 60) {
      aqi = 50 + (pm25 - 30) * 1.66;
    }
    else if (pm25 > 60 && pm25 <= 90) {
      aqi = 100 + (pm25 - 60) * 3.333;
    }
    else if (pm25 > 90 && pm25 <= 120) {
      aqi = 200 + (pm25 - 90) * (3.333);
    }
    else if (pm25 > 120 && pm25 <= 250) {
      aqi = 300 + (pm25 - 120) * (0.769);
    }
    else if (pm25 > 250) {
      aqi =  400 + (pm25 - 250) * (0.769);
    }
    Serial.print("AQI ");
    Serial.println(aqi);

    Serial.println();
    Serial.println("-----------------------------------------------------------------------");
    Serial.println();
    delay(2000);

  }
}

void readTvocSensor() {
  if (! sgp.IAQmeasure()) {
    Serial.println("Measurement failed");
    return;
  }
  Serial.print("TVOC ");
  float x = sgp.TVOC;
  tvoc= (x *3.2430)/1000;
  Serial.print(tvoc); Serial.print(" ppb\t");
   Serial.print("co2(ppm):");
//    co2 = sgp.eCO2; Serial.println(co2);
  Serial.print("eCO2 "); Serial.print(sgp.eCO2); Serial.println(" ppm");

}
void readCo2Sensor() {
  if (airSensor.dataAvailable())
  {
    Serial.print("co2(ppm):");
    //    Serial.print(airSensor.getCO2());
    co2 = airSensor.getCO2(); Serial.println(co2);

    temp = airSensor.getTemperature();
    Serial.print(" temp(C):");
    Serial.print(temp);
    hum =  airSensor.getHumidity();
    Serial.print(" humidity(%):");
    Serial.print( hum);

    Serial.println();
  }
}

void sendAQI() {
  String Command = "aqiData.txt=\"" + String(aqi) + "\"";
  Serial2.print(Command);
  endNextionCommand();
  String aqiStatus = "";
  if (aqi >= 400)
    aqiStatus = "Severe";
  else if (aqi > 301 && aqi < 400)
    aqiStatus = "Very Poor";
  else if (aqi > 201 && aqi <= 300)
    aqiStatus = "Poor";
  else if (aqi > 100 && aqi <= 200)
    aqiStatus = "Moderately Polluted";
  else if (aqi > 50 && aqi <= 100)
    aqiStatus = "Satisfactory";
  else if (aqi >= 0 && aqi <= 50)
    aqiStatus = "Good";
  String CMD = "aqiStatus.txt=\"" + String(aqiStatus) + "\"";
  Serial2.print(CMD);
  endNextionCommand();
}
void sendTemp() {
  String Command = "tempData.txt=\"" + String(temp) + "\"";
  Serial2.print(Command);
  endNextionCommand();
  String tempStatus = "";
  if (temp > 50)
    tempStatus = "Very Hot";
  else if (temp >= 30 && temp < 50)
    tempStatus = "Hot";
  else if (temp >= 15 && temp < 30)
    tempStatus = "Cool";
  else if (temp >= 0 && temp < 15)
    tempStatus = "Very Cold";

  String CMD = "tempStatus.txt=\"" + String(tempStatus) + "\"";
  Serial2.print(CMD);
  endNextionCommand();
}
void sendTvoc() {
  String Command = "tvocData.txt=\"" + String(tvoc) + "\"";
  Serial2.print(Command);
  endNextionCommand();
  String tvocStatus = "";
   if (tvoc >= 4.5)
    tvocStatus = "Severe";
  else if (tvoc >= 3.00 && tvoc < 4.5)
    tvocStatus = "Very Poor";
  else if (tvoc >= 1.80 && tvoc < 3.00)
    tvocStatus = "Poor";
  else if (tvoc >= 0.48 && tvoc < 1.80)
    tvocStatus = "Moderately Polluted";
  else if (tvoc >= 0.18 && tvoc < 0.48)
    tvocStatus = "Satisfactory";
  else if (tvoc >= 0 && tvoc < 0.18)
    tvocStatus = "Good";


  String CMD = "tvocStatus.txt=\"" + String(tvocStatus) + "\"";
  Serial2.print(CMD);
  endNextionCommand();
}
void sendHum() {
  String Command = "humData.txt=\"" + String(hum) + "\"";
  Serial2.print(Command);
  endNextionCommand();
  String humStatus = "";
  if (hum >= 70)
    humStatus = "Very High";
  else if (hum > 50 && hum < 70)
    humStatus = "High";
  else if (hum >= 30 && hum <= 50)
    humStatus = "Good";
  else if (hum >= 0 && hum < 30)
    humStatus = "Poor";
  String CMD = "humStatus.txt=\"" + String(humStatus) + "\"";
  Serial2.print(CMD);
  endNextionCommand();

}
void sendPm25() {
  String Command = "pm25Data.txt=\"" + String(pm25) + "\"";
  Serial2.print(Command);
  endNextionCommand();
  String pm25Status = "";
  if (pm25 >= 250)
    pm25Status = "Severe";
  else if (pm25 > 120 && pm25 < 250)
    pm25Status = "Very Poor";
  else if (pm25 > 90 && pm25 <= 120)
    pm25Status = "Poor";
  else if (pm25 > 60 && pm25 <= 90)
    pm25Status = "Moderately Polluted";
  else if (pm25 > 30 && pm25 <= 60)
    pm25Status = "Satisfactory";
  else if (pm25 >= 0 && pm25 <= 30)
    pm25Status = "Good";
  String CMD = "pm25Status.txt=\"" + String(pm25Status) + "\"";
  Serial2.print(CMD);
  endNextionCommand();


}
void sendPm10() {
  String Command = "pm10Data.txt=\"" + String(pm10) + "\"";
  Serial2.print(Command);
  endNextionCommand();
  String pm10Status = "";
  if (pm10 >= 430)
    pm10Status = "Severe";
  else if (pm10 > 350 && pm10 < 430)
    pm10Status = "Very Poor";
  else if (pm10 > 250 && pm10 <= 350)
    pm10Status = "Poor";
  else if (pm10 > 100 && pm10 <= 250)
    pm10Status = "Moderately Polluted";
  else if (pm10 > 50 && pm10 <= 100)
    pm10Status = "Satisfactory";
  else if (pm10 >= 0 && pm10 <= 50)
    pm10Status = "Good";
  String CMD = "pm10Status.txt=\"" + String(pm10Status) + "\"";
  Serial2.print(CMD);
  endNextionCommand();
}
void sendPm1() {
  String Command = "pm1Data.txt=\"" + String(pm1) + "\"";
  Serial2.print(Command);
  endNextionCommand();
  String pm1Status = "";
  if (pm1 >= 250)
    pm1Status = "Severe";
  else if (pm1 > 120 && pm1 < 250)
    pm1Status = "Very Poor";
  else if (pm1 > 90 && pm1 <= 120)
    pm1Status = "Poor";
  else if (pm1 > 60 && pm1 <= 90)
    pm1Status = "Moderately Polluted";
  else if (pm1 > 30 && pm1 <= 60)
    pm1Status = "Satisfactory";
  else if (pm1 >= 0 && pm1 <= 30)
    pm1Status = "Good";
  String CMD = "pm1Status.txt=\"" + String(pm1Status) + "\"";
  Serial2.print(CMD);
  endNextionCommand();
}
void sendCo2() {
  String Command = "co2Data.txt=\"" + String(co2) + "\"";
  Serial2.print(Command);
  endNextionCommand();
  String co2Status = "";
  if (co2 >= 2500)
    co2Status = "Severe";
  else if (co2 > 1400 && co2 < 2500)
    co2Status = "Very Poor";
  else if (co2 > 1100 && co2 <= 1400)
    co2Status = "Poor";
  else if (co2 > 900 && co2 <= 1100)
    co2Status = "Moderately Polluted";
  else if (co2 > 750 && co2 <= 900)
    co2Status = "Satisfactory";
  else if (co2 >= 0 && co2 <= 750)
    co2Status = "Good";

  String CMD = "co2Status.txt=\"" + String(co2Status) + "\"";
  Serial2.print(CMD);
  endNextionCommand();
}


void sendToNextion() {
  sendTemp();
  sendTvoc();
  sendHum();
  sendPm25();
  sendCo2();
  sendAQI();
  sendPm1();
  sendPm10();
}

void done() {
  Serial.print("Done!");
}

void buttonInit() {
  connectWiFi.attachPush(connectWiFi_Press, &connectWiFi);
  FrgtWifi.attachPush(FrgtWifi_Press, &FrgtWifi);
  wifiButton.attachPush(wifiButton_Press, &wifiButton);
  about.attachPush(about_Press, &about);
  screenOK.attachPush(screenOK_Press, &screenOK);
    m2mButton.attachPush(m2mButton_Press, &m2mButton);
    dataStatusButton.attachPush(dataStatusButton_Press, &dataStatusButton);
  updateOTA.attachPush(updateOTA_Press, &updateOTA);
  confirmReset.attachPush(confirmReset_Press, &confirmReset);
  alarmOK.attachPush(alarmOK_Press, &alarmOK);
  alarmOff.attachPush(alarmOff_Press, &alarmOff);
  alarmButton.attachPush(alarmButton_Press, &alarmButton);
  turnAlarm.attachPop(turnAlarm_release, &turnAlarm);
}
void setup()
{

  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0);
  int16_t ret;
  uint8_t auto_clean_days = 4;
  uint32_t auto_clean;
  nexInit();
  Serial.begin(115200);
  Serial2.begin(115200);
  rs485Serial.begin(9600);
  Serial.println("SCD30 Example");
  Wire.begin();
  SPIFFS.begin(true);

  pinMode(MASTER_EN , OUTPUT);
  digitalWrite(MASTER_EN , HIGH);
  pinMode(buzzerPin, OUTPUT);
  digitalWrite(buzzerPin , LOW);
  EasyBuzzer.setPin(buzzerPin);

  connectWifi();
  DeviceID();

  if (! sgp.begin()) {
    Serial.println("Sensor not found :(");
    while (1);
  }
  Serial.print("Found SGP30 serial #");
  Serial.print(sgp.serialnumber[0], HEX);
  Serial.print(sgp.serialnumber[1], HEX);
  Serial.println(sgp.serialnumber[2], HEX);

  bool status = airSensor.begin(0x61);
  if (!status)
  {
    Serial.println("Air sensor not detected. Please check wiring. Freezing...");
    while (1)
      ;
  }
  sensirion_i2c_init();
  Serial.print("sps30    "); Serial.println(sps30_probe());
  while (sps30_probe() != 0) {
    Serial.print("SPS sensor probing failed\n");
    delay(500);
  }

  ret = sps30_set_fan_auto_cleaning_interval_days(auto_clean_days);
  if (ret) {
    Serial.print("error setting the auto-clean interval: ");
    Serial.println(ret);
  }

  ret = sps30_start_measurement();
  if (ret < 0) {
    Serial.print("error starting measurement\n");
  }

  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
  printLocalTime();
  _fuelGauge1.begin();
  _fuelGauge2.begin();

  //RS485
  node.begin(1, rs485Serial);            //Slave ID as 1
  node.preTransmission(preTransmission);         //Callback for configuring RS-485 Transreceiver correctly
  node.postTransmission(postTransmission);

  buttonInit();

  readTvocSensor();
  readCo2Sensor();
  readPmSensor();
  sendToNextion();

  //create a task that will be executed in the Task1code() function, with priority 1 and executed on core 0
  xTaskCreatePinnedToCore(
    Task1code,   /* Task function. */
    "Task1",     /* name of task. */
    10000,       /* Stack size of task */
    NULL,        /* parameter of the task */
    1,           /* priority of the task */
    &Task1,      /* Task handle to keep track of created task */
    0);          /* pin task to core 0 */
  delay(500);

  //create a task that will be executed in the Task2code() function, with priority 1 and executed on core 1
  xTaskCreatePinnedToCore(
    Task2code,   /* Task function. */
    "Task2",     /* name of task. */
    10000,       /* Stack size of task */
    NULL,        /* parameter of the task */
    1,           /* priority of the task */
    &Task2,      /* Task handle to keep track of created task */
    1);          /* pin task to core 1 */
  delay(500);

}

void Task1code( void * pvParameters ) {
  Serial.print("Task1 running on core ");
  Serial.println(xPortGetCoreID());

  for (;;) {
    //    Serial.println("<--------------------------IN TASK 1-------------------------->");
    unsigned long currentMillis = millis();
    printLocalTime();  
    readTvocSensor();
    if (currentMillis - oldMillisSensor >= 10000) {
      oldMillisSensor = currentMillis;
      readTvocSensor();
      readCo2Sensor();
      readPmSensor();
      sendToNextion();
    }

    if (currentMillis -  oldMillisBtry >= 5000) {
      oldMillisBtry = currentMillis;

      Serial.println("MAX17043:");
      displayFuelGauge(_fuelGauge1);
      Serial.println();
      Serial.println("MAX17044:");
      displayFuelGauge(_fuelGauge2);
    }

    if (currentMillis - oldMillis >= 60000) {
      oldMillis = currentMillis;
      sendToServer();
    }

    wifiConnectStatus();
    if (wifiStatus == true && connectWifiStatus == true) {
      Serial.println("Wifi Status");
      Serial2.write("page wifistatus"); endNextionCommand();

      wifiStatus = false;
      connectWifiStatus = false;
    }

    if (WiFi.status() == WL_CONNECTED) {

      if (!isConnected) {

        Serial.print("WiFi connected to  "); Serial.println(wifiSSID);
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP());
        Serial.println("Connected");
        sendToServer();
        String b = "Connected ";
        String Command1 = "status.txt=\"" + String(b) + "\"";
        Serial2.print(Command1);
        endNextionCommand();

        String wifiCMD = "wifiName.txt=\"" + String(wifiSSID) + "\"";
        Serial2.print(wifiCMD);
        endNextionCommand();
        isConnected = true;

      }
    }
    else {
      isConnected = false;
      String wifiCommand = "wifiPic.pic=" + String(18);
      Serial2.print(wifiCommand);
      endNextionCommand();

      setupWifi();
    }

    String wifiCMD = "wifiName.txt=\"" + String(wifiSSID) + "\"";
    Serial2.print(wifiCMD);
    endNextionCommand();
    if (alarmState == true) {
      Serial.println("in alarm state");
      Serial.print(hourAlarm); Serial.print(" ==== "); Serial.println(hr.toInt());
      Serial.print(minAlarm); Serial.print(" ==== "); Serial.println(mins.toInt());

      if (hourAlarm == hr.toInt() && minAlarm == mins.toInt() ) {
        Serial.println("<--------------------------Alarm On-------------------------->");

        EasyBuzzer.beep(
          frequency,    // Frequency in hertz(HZ).
          onDuration,   // On Duration in milliseconds(ms).
          offDuration,  // Off Duration in milliseconds(ms).
          beeps,      // The number of beeps per cycle.
          pauseDuration,  // Pause duration.
          cycles,     // The number of cycle.
          done      // [Optional] Callback. A function to call when the sequence ends.
        );

        alarmState = false;
      }

    }
    EasyBuzzer.update();

    delay(1000);
  }
}
void Task2code( void * pvParameters ) {
  Serial.print("Task2 running on core ");
  Serial.println(xPortGetCoreID());

  for (;;) {
    //   Serial.println("<--------------------------IN TASK 2-------------------------->");
    nexLoop(nex_listen_list);
    unsigned long currentMillis = millis();
    bool dataSta = false;
    sendToModbus();
    // Serial.print("Task 2  millis  ");Serial.println(currentMillis);
    if (dataStatus == true) {
      readTvocSensor();
      readCo2Sensor();
      readPmSensor();
      sendToNextion();
      sendToModbus();
      Serial.println("<--------------------------sendToModbus-------------------------->");
    
      dataSta = true;
      //      Serial.print("Task 2  currentMillis  ");Serial.println(currentMillis);
      //     Serial.print("Task 2  oldMillis  ");Serial.println(currentMillis);
      //      Serial.print("Task 2  millis  ");Serial.println(currentMillis-oldMillis);

      if (currentMillis - oldMillisMdbs >= 100000) {
        oldMillisMdbs = currentMillis;
        //        sendToModbus();
        //        Serial.println("<--------------------------sendToModbus-------------------------->");
      }

    }
    if (dataSta == true && dataStatus == false) {
       readTvocSensor();
      readCo2Sensor();
      readPmSensor();
      sendToNextion();
      dataSta == false;
    }
    delay(1000);
  }
}


void loop() {
  vTaskDelete(NULL);
}
